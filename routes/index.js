const express = require("express");
const router = express.Router();
const axios = require("axios");

// ************* BACKEND *****************

router.get("/api/v1/products", (req, res) => {
  function responseProducts() {
    return axios.get(`http://localhost:3002/products`);
  }
  const responseAxios = responseProducts();

  responseAxios.then((response) => {
    // res.send(console.log(response.data));
    res.send(response.data);
    // res.render("home", response);
  });
});

router.get("/api/v1/products/:id", (req, res) => {
  let { id } = req.params;

  apiProducts.then((response) => {
    let responseIndex = response.data.findIndex((item) => item.id == id);
    res.json(response.data[responseIndex]);
  });
});

router.post("/api/v1/user/", (req, res) => {
  let { name, email, avatar, password, product_id } = req.body;

  const project = {
    name,
    email,
    avatar,
    password,
    product_id,
  };
  function responseUserPost() {
    return axios.post("http://localhost:3002/user", project);
  }
  const responseAxios = responseUserPost();

  responseAxios.then((response) => {
    response.data.push(project);
  });
  return res.json(project);
});

router.get("/api/v1/user/:id", (req, res) => {
  let { id } = req.params;

  function responseUser() {
    return axios.get(`http://localhost:3002/user`);
  }
  const responseAxios = responseUser();

  responseAxios.then((response) => {
    let responseIndex = response.data.findIndex((item) => item.id == id);
    res.json(response.data[responseIndex]);
  });
});

router.put("/api/v1/user/:id", (req, res) => {
  let { id } = req.params;
  let { name } = req.body;

  const project = {
    name,
  };
  function responseUserPost() {
    return axios.post(`http://localhost:3002/user/${id}`, project);
  }
  responseAxios = responseUserPost();

  responseAxios.then((response) => {
    response.data.push(project);
  });
  return res.json(project);
});

router.get("/api/v1/user/:id/posts", (req, res) => {
  let { id } = req.params;
  let getUsers;
  let getPosts;

  function idUser() {
    return axios.get(`http://localhost:3002/user?product_id=${id}`);
  }
  getUsers = idUser();

  function idPosts() {
    return axios.get(`http://localhost:3001/posts`);
  }
  getPosts = idPosts();

  // pegar id do user e do posts e comparar se são o mesmo e mostrar o resultado
  // let projectIndex = arrayProjects.findIndex((item) => item.id == id);

  getPosts.then((response) => {
    getUsers.then((body) => console.log(body.data));

    let responseIndex = response.data.findIndex(
      (item) => item.products[id] == id
    );
    res.json(response.data[responseIndex]);
    // res.json(response.data);
  });
});
// router.get("/api/v1/user/:idUser/posts/:idPosts", (req, res) => {});

router.get("/", (req, res) => {
  res.render("home");
});

module.exports = router;
