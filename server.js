const app = require("./app");

require("dotenv").config({ path: "variables.env" });

app.set("port", process.env.PORT || 3333);
const server = app.listen(app.get("port"), () => {
  console.log("Server Start: " + server.address().port);
});
