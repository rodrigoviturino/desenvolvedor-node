const express = require("express");
const mustache = require("mustache-express");
const router = require("./routes/index");

const app = express();

app.use(express.json());

// Configurações
app.use("/", router);
// app.use("/painel", adminRouter);

// Template Engine
app.engine("mst", mustache());
app.set("view engine", "mst");
app.set("views", __dirname + "/views");

module.exports = app;
